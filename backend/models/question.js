const mongoose = require('mongoose');

// User Schema
const QuestionSchema = mongoose.Schema({
	question: {
		type: String,
        required: true
	},
    username: {
        type: String,
        required: true
    },
	likes: mongoose.Schema.Types.Mixed,
	dislikes: mongoose.Schema.Types.Mixed
});

const Question = mongoose.model('questions', QuestionSchema);

module.exports.getQuestionById = (id) => {
    let query = {_id: id};
	return Question.findOne(query);
};

module.exports.getMyQuestions = (username) => {
    const query = { username : username };
    return Question.find(query);
};

module.exports.getQuestions = () => {
    return Question.find({});
};

module.exports.searchQuestions = (request) => {
    return Question.find({}).skip(request.skip).limit(request.limit);
};

module.exports.createQuestion = (question) => {
    let entry = new Question();
    entry.question = question.question;
    entry.username = question.username;
    entry.dislikes = question.dislikes;
    entry.likes = question.likes;
    return entry.save();
};

module.exports.Question = Question;