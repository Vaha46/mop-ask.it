const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UserSchema = mongoose.Schema({
	name: {
		type: String
	},
	username: {
		type: String,
		required: true,
		unique: true
	},
	email: {
		type: String,
		required: true,
        unique: true
	},
	password: {
		type: String,
		required: true
	}
});

const User = module.exports = mongoose.model('users', UserSchema);

module.exports.getUserById = function(id, callback) {
	User.findById(id, callback);
};

module.exports.getUserByEmail = function(email, callback) {
	const query = {
		email : email
	};
	User.findOne(query, callback);
};

module.exports.getAllUsers = (request) => {
    return User.find({});
};

module.exports.addUser = function(newUser, callback) {
	bcrypt.genSalt(10, (err, salt) => {
		bcrypt.hash(newUser.password, salt, (err, hash) => {
			let entity = new User();
			entity.password = hash;
			entity.name = newUser.name;
			entity.username = newUser.username;
			entity.email = newUser.email;
            entity.save(callback);
		});
	});
};

module.exports.comparePassword = function(candidatePassword, hash, callback) {
	bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
		if(err) throw err;
		callback(null, isMatch);
	});
};