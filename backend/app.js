const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

// Connect to database
mongoose.connect(config.database).then(
    () => {
        console.log("Database successfully established.");
        const app = express();
        const port = 4000;
        const users = require('./routes/users');
        const questions = require('./routes/questions');
        app.use(cors());
        app.use(bodyParser.json());
        app.use(passport.initialize());
        app.use(passport.session());
        require('./config/passport')(passport);

        console.log("Serve users");
        app.use('/users', users);

        console.log("Serve questions");
        app.use('/questions', questions);

        app.get("/ping", (req, res) => {
            res.send(200, "Server working, time: " + new Date());
        });

        app.listen(port, () => {
            console.log('Server is started on port ' + port);
        });
    },
    (err) => {
        console.log("Failed to create connection.", err);
    }
);