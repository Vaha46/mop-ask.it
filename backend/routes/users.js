const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');

//Register
router.post('/register', (req, res, next) => {

	User.addUser(req.body, (err, user) => {
		if (err) {
			res.json({ success: false, msg: 'Failed to register user' });
		} else {
			res.json({ success: true, msg: 'User registered' });
		}
	})
});

router.post('/all', (req, res, next) => {
    User.getAllUsers(req.body).then(
        (users) => {
            res.status(200).json(users);
        },
        (err) => {
            res.status(400).send(err);
        }
	)
});

//Authenticate
router.post('/authenticate', (req, res, next) => {
	const email = req.body.email;
	const password = req.body.password;

	User.getUserByEmail(email, (err, user) => {
		if (err) throw err;
		if (!user) {
			return res.json({ success: false, msg: 'User not found' });
		}

		User.comparePassword(password, user.password, (err, isMatch) => {
			if (err) throw err;
			if (isMatch) {
				const token = jwt.sign(user.toJSON(), config.secret, {
					expiresIn: 604800 // 1 week
				});
				res.json({
					success: true,
					token: 'jwt ' + token,
					user: {
						id: user._id,
						name: user.name,
						username: user.username,
						email: user.email
					}
				});
			} else {
				return res.json({ success: false, msg: 'Wrong password' });
			}
		});
	});
});

module.exports = router;