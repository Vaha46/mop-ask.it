const express = require('express');
const router = express.Router();
const passport = require('passport');
const Question = require('../models/question');

router.use(function (req, res, next) {
    console.log('Time: ', Date.now());
    next()
});

router.get('/all', (req, res, next) => {
	console.log("Find questions");
    Question.getQuestions().then(
        (questions) => {
            res.status(200).json(questions);
        },
        (err) => {
            res.status(400).send(err);
        }
	)
});

router.post('/search', (req, res, next) => {
	console.log("Find questions");
    Question.searchQuestions(req.body).then(
        (questions) => {
            res.status(200).json(questions);
        },
        (err) => {
            res.status(400).send(err);
        }
	)
});

router.post('/', (req, res, next) => {
	console.log("Create question");
    Question.createQuestion(req.body).then(
        (questions) => {
            res.status(200).json(questions.toJSON());
        },
        (err) => {
            res.status(400).send(err);
        }
	)
});

router.get('/:id', (req, res, next) => {
    console.log("Find question by id");
    Question.getQuestionById(req.params.id).then(
        (question) => {
            res.status(200).json(question.toJSON());
        },
        (err) => {
            res.status(400).send(err);
        }
    )
});

router.patch('/:id/like', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("Like question");
    let username = req.user.username;
    Question.getQuestionById(req.params.id).then(
        (question) => {
            if (!question.likes) { question.likes = []; }
            if (!question.dislikes) { question.dislikes = []; }
            if (question.dislikes.indexOf(username) > -1) {
                question.dislikes.splice(question.dislikes.indexOf(username), 1);
            }
            if (question.likes.indexOf(username) < 0) {
                question.likes.push(username);
            }
            question.markModified('dislikes');
            question.markModified('likes');
            question.save();
            res.status(200).json(question.toJSON());
        },
        (err) => {
            res.status(400).send(err);
        }
    )
});

router.patch('/:id/dislike', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log("Like question");
    let username = req.user.username;
    Question.getQuestionById(req.params.id).then(
        (question) => {
            if (!question.likes) { question.likes = []; }
            if (!question.dislikes) { question.dislikes = []; }
            if (question.likes.indexOf(username) > -1) {
                question.likes.splice(question.likes.indexOf(username), 1);
            }
            if (question.dislikes.indexOf(username) < 0) {
                question.dislikes.push(username);
            }
            question.markModified('dislikes');
            question.markModified('likes');
            question.save();
            res.status(200).json(question.toJSON());
        },
        (err) => {
            res.status(400).send(err);
        }
    )
});

router.use(function (req, res) {
    res.status(404).send('Resource not found');
});

module.exports = router;