const mainReducer = (state = null, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS": {
            return {
                ...state,
                redirectTo: '/',
                isLoggedIn: true
            }
        }
        case "LOGIN_FAILED": {
            return {
                ...state,
                message: action.error,
                isLoggedIn: false
            }
        }
        case "USER_REGISTERED_SUCCESS": {
            return {
                ...state,
                redirectTo: '/login',
            }
        }
        case "USER_REGISTERED_FAILED": {
            return {
                ...state,
                message: action.error
            }
        }
        case "QUESTION_ADDED": {
            return {
                ...state,
                isAdded: action.response
            }
        }
        case "QUESTION_NOT_ADDED": {
            return {
                ...state,
                isAdded: false,
                message: action.error
            }
        }
        case "LOGOUT_SUCCESS": {
            return {
                ...state,
                isLoggedIn: action.response,
                redirectTo: '/'
            }
        }
        case "QUESTIONS_FOUND": {
            return {
                ...state,
                questions: action.response
            }
        }
        case "QUESTIONS_NOT_FOUND": {
            return {
                ...state,
                message: action.error
            }
        }
        case "LIKED_QUESTIONS_FOUND": {
            return {
                ...state,
                likedQuestions: action.response
            }
        }
        case "LIKED_QUESTIONS_NOT_FOUND": {
            return {
                ...state,
                message: action.error
            }
        }
        case "ACTIVE_USERS_FOUND": {
            return {
                ...state,
                mostActiveUsers: action.response
            }
        }
        case "ACTIVE_USERS_NOT_FOUND": {
            return {
                ...state,
                message: action.error
            }
        }
        case "QUESTION_LIKED": {
            return {
                ...state,
                likeStatus: action.response
            }
        }
        case "QUESTION_NOT_LIKED": {
            return {
                ...state,
                message: action.error
            }
        }
        case "QUESTION_FOUND": {
            return {
                ...state,
                question: action.response
            }
        }
        case "QUESTION_NOT_FOUND": {
            return {
                ...state,
                message: action.error
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
};

export default mainReducer;