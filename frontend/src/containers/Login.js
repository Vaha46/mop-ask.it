import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import '../css/Login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            redirectTo: props.redirectTo,
            isValidated: false
        }
    }

    handleOnLogin(e) {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        };
        this.props.loginUser(user);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isValidated: nextProps.redirectTo
        });
    }

    render() {
        return (
            <div className="Login">
                <form className="form-signin" onSubmit={this.handleOnLogin}>
                    <img className="mb-4" src={require('../images/logo.svg')} alt="Ask.it" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-bold">ASK<span className="dot">.it</span></h1>
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <input onChange={e => this.setState({ email: e.target.value })} type="email" id="inputEmail" className="form-control" placeholder="Email address" required="" autoFocus="" />
                    <br />
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <input onChange={e => this.setState({ password: e.target.value })} type="password" id="inputPassword" className="form-control" placeholder="Password" required="" />
                    <hr />
                    <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.handleOnLogin.bind(this)}>Sign in</button>
                    <hr />
                    <Link to={'/register'}>Click here to register</Link>
                    <p className="mt-5 mb-3 text-muted">© 2017-2018</p>
                </form>
                {this.state.isValidated && (
                    <Redirect to={'/'} />
                )}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(Login);