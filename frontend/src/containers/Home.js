import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import { Jumbotron } from '../components/Jumbotron';
import { Navbar } from '../components/Navbar';
import { QuestionBox } from '../components/QuestionBox';
import '../css/Home.css';
import { Redirect } from "react-router-dom";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            mostLiked: [],
            count: 20,
            defaultCount: 5,
        }
    }

    componentWillMount() {
        this.props.searchQuestions({ skip: 0, limit: 2 });
        this.props.mostLikedQuestions({ skip: 0, limit: 5 });
        this.props.activeUsers({ skip: 0, limit: 5 });
    }

    loadMoreQuestions() {
        this.props.searchQuestions({ skip: 0, limit: this.props.questions.length + 2 });
    }

    reloadQuestions() {
        setTimeout(() => {
            this.props.searchQuestions({ skip: 0, limit: this.props.questions.length });
        }, 200);
    }

    handleCount() {
        let count = this.state.defaultCount;
        count = count + this.state.count;
        this.setState({ count });
    }

    render() {
        let questions = this.props.questions ? this.props.questions : [];        
        let mostLiked = this.props.likedQuestions ? this.props.likedQuestions : [];
        let mostActiveUsers = this.props.mostActiveUsers ? this.props.mostActiveUsers: [];
        
        let token = localStorage.getItem("jwt-token");
        if (!token) {
            return <Redirect to={'/login'} />;
        }
        return (
            <div className="Home">
                <Navbar logOut={this.props.logoutSesion} />
                <Jumbotron title="Welcome" subtitle="Where all answers are answerd, and ideas are born" />
                <div className="container-fluid mainContent">
                    <div className="row">
                        <div className="col-sm-12 col-md-9 col-lg-9">
                            <QuestionBox value={questions} search={() => this.reloadQuestions()} count={questions.length} like={this.props.likeQuestion} dislike={this.props.dislikeQuestion} />
                            <div className="loadMoreBtn">
                                <button className="btn" onClick={() => { this.loadMoreQuestions() }}>Load more...</button>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-2 col-lg-3">
                            <div className="row sideBar">
                                <div className="col-sm-12 mostLiked">
                                    <h5><img className="img-responsive thumbsUp" alt="Logo" src={require('../images/thumbs-up.svg')} /> Most liked</h5>
                                    <hr />
                                    <ul>{
                                        mostLiked.map((val, index) => {
                                            return ((index < 5) ? <li key={index}><span>&#9900; </span>{index + 1}-ID {val.username}</li> : '')
                                        })
                                    }
                                    </ul>
                                </div>
                                <div className="col-sm-12 activeUsers">
                                    <h5><img className="img-responsive thumbsUp" alt="Logo" src={require('../images/users.svg')} /> Most active</h5>
                                    <hr />
                                    <ul>{
                                        mostActiveUsers.map((val, index) => {
                                            return (<li key={index}><span>&#9900;</span> {val.username}</li>)
                                        })
                                    }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(Home);