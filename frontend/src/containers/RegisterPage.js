import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import { Link } from 'react-router-dom';
import '../css/RegisterPage.css';

class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            username: '',
            name: '',
            passConfirm: '',
            isPasswordMatched: true
        }
    }

    handleOnRegister(e) {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password,
            username: this.state.username,
            passConfirm: this.state.passConfirm,
            name: this.state.name,
        };

        if (this.state.password === this.state.passConfirm) {
            (this.props.registerUser(user)).then((response) => {
                console.log(response);
            });
            this.setState({ isPasswordMatched: true });
        } else {
            this.setState({ isPasswordMatched: false });
        }
    }

    render() {
        return (
            <div className="RegisterPage">
                <form className="form-register needs-validation" onSubmit={this.handleOnRegister}>
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <img className="mb-4" src={require('../images/logo.svg')} alt="Ask.it" width="72" height="72" />
                            <h1 className="h3 mb-3 font-weight-bold">ASK<span className="dot">.it</span></h1>
                            <h2>Register</h2>
                            <hr />
                        </div>
                    </div>

                    {!this.state.isPasswordMatched ? <h5 style={{ color: 'red' }}>Passwords are not matching!</h5> : ''}
                    <div className="row">
                        <div className="col-md-3 field-label-responsive">
                            <label>Username</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div className="input-group-addon"></div>
                                    <input type="text" onChange={e => this.setState({ username: e.target.value })} name="username" className="form-control" id="username" placeholder="Username" required autoFocus="" />
                                </div>

                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-control-feedback">
                                <span className="text-danger align-middle">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 field-label-responsive">
                            <label>Name</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div className="input-group-addon"></div>
                                    <input type="text" onChange={e => this.setState({ name: e.target.value })} name="name" className="form-control" id="name" placeholder="Name" required autoFocus="" />
                                </div>

                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-control-feedback">
                                <span className="text-danger align-middle">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 field-label-responsive">
                            <label>E-Mail Address</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div className="input-group-addon"><i className="fa fa-at"></i></div>
                                    <input type="text" onChange={e => this.setState({ email: e.target.value })} name="email" className="form-control" id="email" placeholder="you@example.com" required="" autoFocus="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="form-control-feedback">
                                <span className="text-danger align-middle">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 field-label-responsive">
                            <label>Password</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group has-danger">
                                <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div className="input-group-addon"><i className="fa fa-key"></i></div>
                                    <input type="password" onChange={e => this.setState({ password: e.target.value })} name="password" className="form-control" id="password" placeholder="Password" required="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 field-label-responsive">
                            <label>Confirm Password</label>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div className="input-group-addon">
                                        <i className="fa fa-repeat"></i>
                                    </div>
                                    <input type="password" onChange={e => this.setState({ passConfirm: e.target.value })} name="password-confirmation" className="form-control" id="password-confirm" placeholder="Password" required="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <button type="submit" className="btn btn-lg btn-success btn-block" onClick={this.handleOnRegister.bind(this)}>Register</button>
                        </div>
                    </div>
                    <hr />
                    <Link to={'/login'}>Click here to go back to login</Link>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(RegisterPage);