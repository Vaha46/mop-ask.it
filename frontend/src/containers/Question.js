import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import { Jumbotron } from '../components/Jumbotron';
import { Navbar } from '../components/Navbar';
import '../css/Home.css';
import {Link} from "react-router-dom";

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            users: [],
            mostLiked: [],
            count: 20,
            defaultCount: 5,
        }
    }

    componentWillMount() {
        this.props.findQuestion(this.props.match.params.id);
    }

    componentDidMount() {
    }

    handleCount() {
        let count = this.state.defaultCount;
        count = count + this.state.count;
        this.setState({ count });
    }

    handleThis() {
        this.props.loadColor();
    }

    render() {
        let props = this.props;
        if (props.question) {
            return (
                <div className="Question">
                    <Navbar />
                    <Jumbotron title="Welcome" subtitle="Where all answers are answerd, and ideas are born" />
                    <div className="container-fluid mainContent">
                        <div className="row">
                            <div className="col-sm-12 col-md-12 col-lg-12">
                                <div className="col-sm-12 col-md-12 col-lg-12 questionBox">
                                    <div className="questionText">
                                        <h5>{props.question.userId}-ID {props.question._id}</h5>
                                        <hr />
                                        <p className="questionsParagraph">{props.question.question}</p>
                                        <hr />
                                        <Link to={'/'}>
                                            <button className="btn btn-secondary readMore float-right">
                                                Back
                                            </button>
                                        </Link>
                                        <button className="btn float-left thumbDwnBtn">
                                            Add answer
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >
                </div >
            );
        } else {
            return <div className="Question">
                <Navbar />
                <Jumbotron title="Welcome" subtitle="Where all answers are answerd, and ideas are born" />
                <div className="container-fluid mainContent">
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <div className="questionText">
                                <p>Fetching data from server.</p>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <Link to={'/'}>
                                <button className="btn btn-secondary readMore float-right">
                                    Back
                                </button>
                            </Link>
                        </div>
                    </div>
                </div >
            </div >
        }
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(Question);