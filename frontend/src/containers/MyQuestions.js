import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import { Jumbotron } from '../components/Jumbotron';
import { Navbar } from '../components/Navbar';
import '../css/Profile.css';

class MyQuestions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: ''
        }
    }
    handleSumbitAnswer(e) {
        e.preventDefault();
        const username = JSON.parse(localStorage.getItem('jwt-user')).username;
        const question = {
            question: this.state.question,
            username: username
        }
        this.props.addQuestion(question);
    }

    render() {
        return (
            <div className="MyQuestions">
                <Navbar logOut={this.props.logoutSesion} />
                <Jumbotron title="Welcome" subtitle="Where all answers are answerd, and ideas are born" />
                <div className="container mainContent">
                    <div className="row">
                        <div className="col-sm-12 text-center">
                            {this.props.isAdded ? <h5>Question Added,thank you.</h5> : ''}
                            <form className="form-signin" onSubmit={this.handleSumbitAnswer}>
                                <div className="form-group row">
                                    <div className="form-group col-sm-12">
                                        <h3>Enter your question: </h3>
                                        <textarea className="form-control" onChange={e => this.setState({ question: e.target.value })} id="exampleTextarea" rows="5" style={{ "resize": "none", "marginTop": "20px" }} />
                                        <input className="btn btn-success form-control" onClick={this.handleSumbitAnswer.bind(this)} type="submit" value="Submit" id="example-text-input" style={{ "width": "75px", "float": "right", "marginTop": "20px" }} />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div >
            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(MyQuestions);