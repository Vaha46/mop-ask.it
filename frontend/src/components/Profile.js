import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/';
import { Jumbotron } from '../components/Jumbotron';
import { Navbar } from '../components/Navbar';
import '../css/Profile.css';

class Profile extends Component {
    render() {
        const userDetails = JSON.parse(localStorage.getItem('jwt-user'));
        return (
            <div className="Profile">
                <Navbar logOut={this.props.logoutSesion} />
                <Jumbotron title="Welcome" subtitle="Where all answers are answerd, and ideas are born" />
                <div className="container mainContent">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-md-12 lead">User profile<hr /></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 text-center">
                                            <img src={require('../images/man.svg')} style={{"height": "100px"}} className="rounded-circle avatar avatar-original" alt="profile" />
                                        </div>
                                        <div className="col-md-8">
                                            <div className="row">
                                                <div className="col-md-6 text-center">
                                                    <span className="text-muted">Name:</span> {userDetails.name}<br />
                                                    <span className="text-muted">Username:</span> {userDetails.username}<br />
                                                    <span className="text-muted">Email:</span> {userDetails.email}<br /><br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps, actionCreators)(Profile);