import React, { Component } from 'react';
import '../css/QuestionBox.css';
import { Link } from "react-router-dom";

export class QuestionBox extends Component {

    like(question) {
        this.props.like(question._id);
        this.props.search();
    }

    dislike(question) {
        this.props.dislike(question._id);
        this.props.search();
    }

    isQuestionLiked(question) {
        let user = JSON.parse(localStorage.getItem("jwt-user"));
        question.likes = question.likes ? question.likes : [];
        return question.likes.indexOf(user.username) > -1;
    }

    isQuestionRated(question) {
        let user = JSON.parse(localStorage.getItem("jwt-user"));
        question.likes = question.likes ? question.likes : [];
        question.dislikes = question.dislikes ? question.dislikes : [];
        let liked = question.likes.indexOf(user.username) > -1;
        let disliked = question.dislikes.indexOf(user.username) > -1;
        return liked || disliked;
    }

    render() {
        if (this.props.value) {
            return (
                <div className="row">
                    {
                        this.props.value.map((question, index) => {
                            question.liked = this.isQuestionLiked(question);
                            question.rated = this.isQuestionRated(question);
                            return ((index < this.props.count) ?
                                <div key={index} className="col-sm-12 col-md-12 col-lg-6 questionBox">
                                    <div className="questionText">
                                        <h5>{index + 1}-ID {question.username}</h5>
                                        <hr />
                                        <p className="questionsParagraph">{question.question}</p>
                                        <hr />
                                        {
                                            question.rated ?
                                                <div>
                                                    {
                                                        question.liked ?
                                                            <button className="btn float-left thumbDwnBtn" onClick={() => { this.dislike(question) }}>
                                                                <img className="img-responsive thumbsDown" alt="Logo" src={require('../images/thumb-down.svg')} />
                                                                &nbsp;&nbsp;You already liked this question. Dislike now.
                                                                </button>
                                                            :
                                                            <button className="btn float-left thumbUpBtn" onClick={() => { this.like(question) }}>
                                                                <img className="img-responsive thumbsUp" alt="Logo" src={require('../images/thumbs-up.svg')} />
                                                            </button>
                                                    }
                                                </div>
                                                :
                                                <div>
                                                    <button className="btn float-left thumbUpBtn" onClick={() => { this.like(question) }}>
                                                        <img className="img-responsive thumbsUp" alt="Logo" src={require('../images/thumbs-up.svg')} />
                                                    </button>
                                                    <button className="btn float-left thumbDwnBtn" onClick={() => { this.dislike(question) }}>
                                                        <img className="img-responsive thumbsDown" alt="Logo" src={require('../images/thumb-down.svg')} />
                                                    </button>
                                                </div>
                                        }
                                        <Link to={'/questions/' + question._id}>
                                            <button className="btn btn-secondary readMore float-right">
                                                Read more...
                                                </button>
                                        </Link>
                                    </div>
                                </div> : ''
                            );
                        })
                    }
                </div>
            );
        } else {
            return <p>Loading questions</p>
        }
    }
}