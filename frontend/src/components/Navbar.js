import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Navbar.css';


export function logOut(props) {
    localStorage.removeItem('jwt-token');
    localStorage.removeItem('jwt-user');
    props.logOut();
}

export const Navbar = (props) => {

    return (
        <div className="Navbar">
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <div className="container">
                    <img className="logo img-responsive" alt="Logo" src={require('../images/chat-bubble.png')} />
                    <Link className="navbar-brand mr-auto" to="/">ASK<span className="dot">.it</span></Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/questions">Questions</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/myquestions">My Questions</Link>
                            </li>
                            <li className="nav-item">
                                <div className="dropdown show">
                                    <div className="nav-link dropdown-toggle" to="" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img className="img-responsive settingsIcon" alt="Logo" src={require('../images/settings.svg')} />
                                    </div>
                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                        <Link className="dropdown-item" to="/profile">Profile</Link>
                                        <Link className="dropdown-item" to="/login" onClick={() => logOut(props)}>Logout</Link>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    );
}