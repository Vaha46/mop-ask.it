import React from 'react';
import '../css/Footer.css';

export const Footer = (props) => {
    return (
        <footer className="footer">
            <div className="container">
                <span className="text-muted">Place where questions are answered.</span>
            </div>
        </footer>
    );
}