import React from 'react';
import { Link } from 'react-router-dom';
import '../css/RegisterPage.css';

export const RegisterPage = (props) => {
    return (
        <div className="RegisterPage">
            <form className="form-register">
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <img className="mb-4" src={require('../images/logo.svg')} alt="Ask.it" width="72" height="72" />
                        <h1 className="h3 mb-3 font-weight-bold">ASK<span className="dot">.it</span></h1>
                        <h2>Register</h2>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 field-label-responsive">
                        <label>Name</label>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div className="input-group-addon"><i className="fa fa-user"></i></div>
                                <input type="text" name="name" className="form-control" id="name" placeholder="John Doe" required="" autoFocus="" />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="form-control-feedback">
                            <span className="text-danger align-middle">
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 field-label-responsive">
                        <label>E-Mail Address</label>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div className="input-group-addon"><i className="fa fa-at"></i></div>
                                <input type="text" name="email" className="form-control" id="email" placeholder="you@example.com" required="" autoFocus="" />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="form-control-feedback">
                            <span className="text-danger align-middle">
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 field-label-responsive">
                        <label>Password</label>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group has-danger">
                            <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div className="input-group-addon"><i className="fa fa-key"></i></div>
                                <input type="password" name="password" className="form-control" id="password" placeholder="Password" required="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 field-label-responsive">
                        <label>Confirm Password</label>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div className="input-group-addon">
                                    <i className="fa fa-repeat"></i>
                                </div>
                                <input type="password" name="password-confirmation" className="form-control" id="password-confirm" placeholder="Password" required="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <button type="submit" className="btn btn-lg btn-success btn-block"><i className="fa fa-user-plus"></i> Register</button>
                    </div>
                </div>
                <hr />
                <Link to={'/login'}>Click here to go back to login</Link>
            </form>
        </div>
    );
}