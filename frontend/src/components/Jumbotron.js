import React from 'react';
import '../css/Jumbotron.css';

export const Jumbotron = (props) => {
    return (
        <div className="jumbotron jumbotron-fluid">
            <div className="container image-text">
                <h1 className="display-3">{props.title}</h1>
                <p className="lead">{props.subtitle}</p>
            </div>
        </div>

    );
}
