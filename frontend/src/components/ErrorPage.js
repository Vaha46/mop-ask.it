import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Login.css';

export const ErrorPage = (props) => {
    return (
        <div className="Login">
            <form className="form-signin">
                <img className="mb-4" src={require('../images/logo.svg')} alt="Ask.it" width="72" height="72" />
                <h1 className="h3 mb-3 font-weight-bold">ASK<span className="dot">.it</span></h1>
                <hr />
                <h1> 404 Page Not Found </h1>
                <hr />
                <button className="btn btn-secondary"><Link to="/login">Click here to go to login page</Link></button>
            </form>
        </div>
    );
}