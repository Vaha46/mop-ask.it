import axios from 'axios';

export function loginUser(user) {
    return (dispatch) => {
        return sendRequest("POST", "/users/authenticate", {
            email: user.email,
            password: user.password
        }).then((response) => {
            localStorage.setItem("jwt-token", response.data.token);
            localStorage.setItem("jwt-user", JSON.stringify(response.data.user));
            dispatch({ type: "LOGIN_SUCCESS", response: response });
        }).catch((error) => {
            dispatch({ type: "LOGIN_FAILED", error: error });
        })
    }
}

export function registerUser(user) {
    return (dispatch) => {
        return sendRequest("POST", "/users/register", {
            email: user.email,
            password: user.password,
            username: user.username,
            name: user.name
        }).then((response) => {
            dispatch({ type: "USER_REGISTERED_SUCCESS", response: response });
        }).catch((error) => {
            dispatch({ type: "USER_REGISTERED_FAILED", error: error });
        })
    }
}

export function searchQuestions(request) {
    return (dispatch) => {
        return sendRequest("POST", "/questions/search", request).then((response) => {
            dispatch({ type: "QUESTIONS_FOUND", response: response.data });
        }).catch((error) => {
            dispatch({ type: "QUESTIONS_NOT_FOUND", error: error });
        })
    }
}

export function mostLikedQuestions(request) {
    return (dispatch) => {
        return sendRequest("POST", "/questions/search", request).then((response) => {
            dispatch({ type: "LIKED_QUESTIONS_FOUND", response: response.data });
        }).catch((error) => {
            dispatch({ type: "LIKED_QUESTIONS_NOT_FOUND", error: error });
        })
    }
}


export function activeUsers(request) {
    return (dispatch) => {
        return sendRequest("POST", "/users/all", request).then((response) => {
            dispatch({ type: "ACTIVE_USERS_FOUND", response: response.data });
        }).catch((error) => {
            dispatch({ type: "ACTIVE_USERS_FOUND_NOT_FOUND", error: error });
        })
    }
}

export function findQuestion(id) {
    return (dispatch) => {
        return sendRequest("get", "/questions/" + id).then((response) => {
            dispatch({ type: "QUESTION_FOUND", response: response.data });
        }).catch((error) => {
            dispatch({ type: "QUESTION_NOT_FOUND", error: error });
        })
    }
}

export function likeQuestion(id) {
    return (dispatch) => {
        return sendRequest("patch", "/questions/" + id + "/like", {}).then((response) => {
            dispatch({ type: "QUESTION_LIKED", response: response.data });
        }).catch((error) => {
            dispatch({ type: "QUESTION_NOT_LIKED", error: error });
        })
    }
}

export function dislikeQuestion(id) {
    return (dispatch) => {
        return sendRequest("patch", "/questions/" + id + "/dislike", {}).then((response) => {
            dispatch({ type: "QUESTION_LIKED", response: response.data });
        }).catch((error) => {
            dispatch({ type: "QUESTION_NOT_LIKED", error: error });
        })
    }
}

export function addQuestion(question) {
    return (dispatch) => {
        return sendRequest("POST", "/questions/", question).then((response) => {
            console.log(response);
            dispatch({ type: "QUESTION_ADDED", response: true });
        }).catch((error) => {
            dispatch({ type: "QUESTION_NOT_ADDED", error: error });
        })
    }
}

export function sendRequest(method, path, data) {
    let url = 'http://192.168.1.9:4000' + path;
    return axios({
        method: method,
        url: url,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem("jwt-token")
        },
        data: data
    })
}

export function logoutSesion() {
    return (dispatch) => {
        dispatch({ type: "LOGOUT_SUCCESS", response: false });
    }
}