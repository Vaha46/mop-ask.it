import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Login from './containers/Login';
import Home from './containers/Home';
import { ErrorPage } from './components/ErrorPage';
import RegisterPage from './containers/RegisterPage';
import Profile from './components/Profile';
import MyQuestions from './containers/MyQuestions';
import './css/App.css';
import Question from "./containers/Question";

class App extends Component {

    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <PrivateRoute exact path='/' component={Home} />
                        <Route path='/login' component={Login} />
                        <Route path='/register' component={RegisterPage} />
                        <Route path='/myquestions' component={MyQuestions} />
                        <PrivateRoute path='/profile' component={Profile} />
                        <PrivateRoute path='/questions/:id' component={Question} />
                        <Route path="*" component={ErrorPage} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

const simulateAuth = {
    isAuthenticated: true,
    authenticate(cb) {
        this.isAuthenticated = true;
        setTimeout(cb, 100);
    },
    signout(cb) {
        this.isAuthenticated = false;
        setTimeout(cb, 100);
    }
};

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            simulateAuth.isAuthenticated ? (
                <Component {...props} />
            ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: props.location }
                        }}
                    />
                )
        }
    />
);

export default App;
